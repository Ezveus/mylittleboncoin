class AdsController < ApplicationController
    def index
        @query = {}
        if params && params[ads_path]
            @query[:kind] = params[ads_path][:kind] unless params[ads_path][:kind].blank?
            @query[:title] = params[ads_path][:title] unless params[ads_path][:title].blank?
            @query[:pseudo] = params[ads_path][:pseudo] unless params[ads_path][:pseudo].blank?
        end
        @ads_number = Ad.count
        @ads = Ad.where(@query).order(created_at: :desc).page(params[:page] || 1)
    end

    def show
        @ad = Ad.find_by_id(params[:id])
    end

    def new
        @ad = Ad.new
    end

    def create
        @ad = Ad.new(params.require(:ad).permit(:kind, :title, :body, :zipcode, :pseudo, :email, :phone, :price))

        if @ad.valid?
            @ad.save

            redirect_to @ad
        else
            render 'new'
        end
    end
end
