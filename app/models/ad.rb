class Ad < ApplicationRecord
    KIND = ["offre", "demande"]

    validates :kind, :title, :body, :zipcode, :pseudo, :email, :phone, presence: true
    validates :price, numericality: {greater_than: 0}, allow_nil: true

    validates_each :email do |record, attr, value|
        record.errors.add( attr, "must be a valid email" ) unless value =~ /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\Z/i
    end

    validates_each :phone do |record, attr, value|
        record.errors.add( attr, "must be a valid french phone number" ) unless value =~ /\A(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}\Z/
    end

    validates_each :kind do |record, attr, value|
        record.errors.add( attr, "must be one of: #{KIND.join( ', ' )}" ) unless KIND.include? value
    end
end
