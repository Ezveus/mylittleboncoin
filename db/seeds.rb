# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
(0...30).each do |idx|
    kind = idx % 2 == 0 ? "offre" : "demande"
    price = idx % 3 == 0 ? nil : rand * 100
    pseudo = "pseudo_#{idx}"
    body = idx % 2 == 0 ? "Test" : %{
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris imperdiet enim id pellentesque rutrum. Duis pellentesque maximus nisi. Nunc varius viverra velit vel fermentum. Duis sodales dignissim libero, non elementum nisl aliquam nec. Ut nec tincidunt massa. Quisque vitae viverra nibh. Donec ut dapibus sem. Nam ullamcorper urna tellus, vehicula ornare ipsum tempor eget. Integer tortor nunc, maximus ut tempus sit amet, blandit sed mauris.

Pellentesque sed diam in tellus tempor porttitor sed nec lectus. Proin a dolor eleifend, aliquam quam quis, maximus orci. Nullam posuere sem sit amet nisi feugiat sagittis. In consectetur augue nisi, vel pharetra nunc commodo eget. Nullam mi lacus, pellentesque in eros ac, vehicula auctor nulla. Phasellus dignissim ornare turpis, eu rutrum ex. Aliquam rhoncus finibus nisi ut efficitur. Vivamus eu sagittis libero. Aenean suscipit ut purus sed fermentum. Aliquam erat volutpat.

Curabitur fermentum, eros vulputate cursus dictum, nisl nisl consectetur justo, sodales porttitor tellus diam vehicula ex. Donec ut lacinia mi, id aliquet velit. Quisque fermentum odio eros, nec gravida diam ornare ut. Nullam congue volutpat arcu sit amet gravida. Praesent elit nisi, elementum auctor auctor a, malesuada eu elit. Duis ante nisl, semper sit amet gravida sit amet, dictum ut lectus. Duis gravida risus id tempor placerat. Phasellus enim arcu, aliquam vitae nisl quis, rhoncus iaculis nulla. Aenean eget aliquet erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ac massa magna. Aliquam rutrum aliquet turpis, gravida volutpat quam rutrum ut.

Cras ut tellus laoreet, dictum erat a, pretium dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ultrices tempus ex molestie mattis. Nulla nisi sapien, cursus a elit vitae, laoreet pretium magna. Vestibulum felis ex, commodo in quam id, consectetur consequat ante. Proin ornare lacus ultricies eleifend varius. Nunc eleifend tortor nec mi luctus, feugiat euismod massa facilisis.

Duis sollicitudin magna libero, at molestie justo sagittis sit amet. Nulla vulputate dictum eros bibendum faucibus. Ut tincidunt pretium pulvinar. Nunc ornare lorem ut eros posuere faucibus. Nunc imperdiet dapibus sapien ac tristique. Cras dictum varius risus nec bibendum. Nullam hendrerit interdum nulla ut porta. Sed pulvinar, lacus quis pellentesque tincidunt, nibh nisl consectetur nulla, id vulputate nisi orci non ante. Vivamus id pellentesque velit. Vivamus at turpis metus. Ut auctor egestas neque, et porttitor ante euismod quis. Maecenas nec venenatis odio. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper, nisi in egestas faucibus, tellus augue ornare lorem, quis fringilla elit tellus vitae dolor.
    }

    Ad.create!(kind: kind, title: "#{kind} ##{idx}", body: body,
               price: price, zipcode: '2A000', pseudo: pseudo,
               email: "#{pseudo}@domain.tld", phone: "0123456789")
end
