class CreateAds < ActiveRecord::Migration[5.1]
  def change
    create_table :ads do |t|
      t.string :kind, null: false
      t.string :title, null: false, index: true
      t.text :body, null: false
      t.decimal :price, index: true
      t.string :zipcode, null: false, index: true
      t.string :pseudo, null: false
      t.string :email, null: false
      t.string :phone, null: false

      t.timestamps
    end
  end
end
