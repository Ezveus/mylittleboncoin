Rails.application.routes.draw do
  resources :ads, except: [:edit, :update, :destroy]

  root to: 'ads#index'
end
