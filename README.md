# README

Réalisé en 3 heures et 30 minutes (environ).

J'ai choisi de reprendre l'ajout, la visualisation et la recherche d'annonces.
C'est en effet la fonctionnalité principale de Bon Coin.

Les grandes étapes correspondent aux commits :

- Initialiser le modèle principal
- Ajouter les vues (index, création et visualisation)
- Ajouter de la pagination sur l'index
- Ajouter une (pseudo) recherche sur l'index
